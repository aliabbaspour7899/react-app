import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Register from './register/Register';
import * as serviceWorker from './serviceWorker';


  ReactDOM.render(
    <Router>
        <Route path='/' exact  component={Register} />
        <Route path='/base' exact  component={App} />
    </Router>
,
    document.getElementById('root')
  );



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
