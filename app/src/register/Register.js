import React from 'react'
import '../index.css'
import './Register.css'
import Body from './body/Body'

const Register = () => {
    return (
        <div id="section-step" className="flex-center">
           <Body /> 
        </div>
    )
}

export default Register
