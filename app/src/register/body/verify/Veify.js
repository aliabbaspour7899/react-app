import React from 'react'
import './Verify.css'
import '../../Register.css'
const Veify = () => { 
    return (
        <div className="colm10 colm11-tab colm margin-auto pad-5-mob">
            <div className="pad-b20">
                <h2 className="font-s30 color6">خوش آمدید</h2>
                <p className="color-darkgray font-s14 align-justify pad-t15">لطفا برای آغاز ثبت نام، شماره تلفن همراه خود را وارد نمایید.
                    توجه فرمایید که کد احراز هویت برای شما ارسال می گردد، بنابراین از درستی شماره خود اطمینان حاصل فرمایید.</p>
            </div>
            <div className="spacer-t5">
                <form method="post">
                    <div className="frm-row pad-5 colm colm9 margin-auto">
                        <label for="id_phone" className="gui-label pad-5">تلفن همراه </label>
                        <label className="relative">
                            <span className="icon-gui flex-center"><i className="fa fa-phone vertical"></i></span>
                            <input dir="ltr" type="tel" placeholder="091212345678" className="gui-input sans-digit englishnum number-input" />
                        </label>
                    </div>
                    <div className="align-left spacer-t40">
                        <button type="submit" className="btn-web colm" >مرحله بعد </button>
                    </div>
                </form>
            </div>
        </div>    
    )
}

export default Veify
