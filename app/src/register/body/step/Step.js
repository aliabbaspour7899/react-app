import React from 'react'
import './Step.css'
import '../../Register.css'

const Step = () => {
    return (
        <div className="rtl">
            <div id="steps">
                <ul>
                    <li>
                        <div className="step active" data-desc="آغاز ثبت نام" align="">1</div>
                    </li>
                    <li>
                        <div className="step" data-desc="اعتبارسنجی" align="">2</div>
                    </li>
                    <li>
                        <div className="step" data-desc="معرفی استارتاپ" align="">3</div>
                    </li>
                    <li>
                        <div className="step" data-desc="اطلاعات تیم" align="">4</div>
                    </li>
                    <li>
                        <div className="step" data-desc="معرفی ویدئویی" align="">5</div>
                    </li>
                    <li>
                        <div className="step" data-desc="پیچ دک" align="">6</div>
                    </li>
                    <li>
                        <div className="step" data-desc="تایید نهایی" align="">7</div>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default Step
