import React from 'react'
import Step from './step/Step'
import Verify from './verify/Veify'
import './Body.css'
import './step/Step.css'

const Body = () => {
    return (
        <div id="section-step-content" className="relative colm8 colm margin-auto pad-40  bg-white spacer-t40 spacer-b40">
            <Step/>
            <div className="colm10 colm margin-auto">
                <div className="spacer-t5">
                    <Verify/>   
                </div>
            </div>
        </div>
    )
}

export default Body
